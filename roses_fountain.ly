\version "2.20.0"

susOn = \sustainOn
susOff = \sustainOff
susRes = \sustainOff \sustainOn

partAR = {
    \tempo "Andante" 4 = 80
    \key as \major
    \mark \default
    r2
    des'16 as' c f bes es c8
    r8. f,,16 as es' bes g'
    bes as g f des4
}

partAL = {
    \key as \major
    r1
    \susOn
    des16 \susRes as' c r4 r16
    bes'16 as g f des4
}

partBR = {
    \clef "treble^8"
    \key b \major
    <dis' dis'>2
    <gis, gis'>4 <ais ais'>8 <b b'>

    <cis cis'>2
    <fis, fis'>

    <dis' dis'>2.

    <bis, bis'>8 \arpeggio
    <cis cis'> \arpeggio
    <dis dis'> \arpeggio
    <eis eis'> \arpeggio
    <bis' bis'>4 \arpeggio r2
}

partBL = {
    \key b \major
    e,,16 \susRes b' e fis
    gis b e fis
    ais b ais fis
    cis b ais fis

    fis, \susRes cis' fis gis
    ais cis eis fis
    ais fis eis cis
    ais gis fis cis

    cis, \susRes gis' cis dis
    eis gis bis cis
    dis eis gis8 r4

    gis16 bis cis8 r4

    <gis,, cis dis gis bis>2\arpeggio
    \susRes
}

partCR = {
    \mark \default
    r8. e,,16
    fis gis ais b
    cis dis e fis
    gis4
}

partCL = {
    e8 \susRes b' fis' e
    b'2
}

partDR = {
    % consider inserting the up-down arpeggio from 'The Hill' here
    <dis eis gis ais>8\arpeggio
       gis dis' dis,
    \repeat unfold 7 { ais' gis dis' dis, }
}

partDL = {
    <dis gis ais cis>4\arpeggio\susRes
    \clef "treble^15"
    dis''' ais gis4
    r dis' ais2
    % maybe extend to include the chord progressions from 'The Hill' here
    r4 dis ais gis
    <cis gis' bis>2 \susRes <dis ais' cis>4 dis,
}

partER = {
    \mark \default
    \clef treble
    \key as \major

    <es, g c>2\arpeggio ~
    q8 es8 bes' as
    <es ges c>4\arpeggio~ q16 f ges bes
    c des ges bes c4

    % is the above a ges or a fis?
    % ges, because it's functioning as a seventh

    <f,, as es'>8\arpeggio des' c des ~
    des as as'4
    <fes, as bes es>8\arpeggio des' c des ~
    des4 as8 bes

    <es, g c>2 ~
    <es g c>8 g c4
    <c, es a>2 \arpeggio ~
    q8 c f4

    <des f c'>8 \arpeggio bes' a bes
    f4 g
    <c, f as>8. g' f8 ~
    f2

    <b, e gis>8. fis' e8 ~ e4 b4
}

partEL = {
    \clef bass
    \key as \major

    as,,,8 \susRes es' as bes
    c  r as es
    as, \susRes es' as bes
    c es ges4

    des,,8 \susRes as' des es
    f r des as
    des, \susRes as' des es
    fes des as des,

    c \susRes g' c d
    es r g, c,
    f, \susRes c' f g
    a r4 f8

    bes,8 \susRes f' bes des
    r des bes f

    <des as'>2. as4 \susRes
    <des, des'>1 \susRes
}

partFR = {
    \set tieWaitForNote = ##t
    \tieDown
    <des f as>8 ~ c' des f
    c'4 <des,, f as bes'>
    <bes des f bes>2
    es''4 des
    <as c es g>1\arpeggio
}

partFL = {
    \tieDown
    <es' bes'>2 \susRes~
    q4 bes
    <es, es'>1 \susRes
    r1
}

\header {
    title = "Rose's Fountain / The Hill"
    subtitle = "from Steven Universe"
    instrument = "Piano"
    composer = "Aivi & Surasshu"
    arranger = "Arranged by Troels Korreman"
    %copyright = "© Cartoon Network"
    tagline = ""
}

\include "alternative_notation.ly"
\score {
    \pointAndClickOff
    \new GrandStaff <<
        \set GrandStaff.connectArpeggios = ##t
        \new KorrStaff \relative { \clef treble
            \set Score.markFormatter = #format-mark-box-alphabet
            \set Staff.printKeyCancellation = ##f
            \set Staff.pedalSustainStyle = #'mixed
            \time 4/4
            \partAR
            \partBR
            \partCR
            \partDR
            \partER
            \partFR
        }
        \new KorrStaff \relative { \clef bass
            \set Staff.printKeyCancellation = ##f
            \set Staff.pedalSustainStyle = #'mixed
            \time 4/4
            \partAL
            \partBL
            \partCL
            \partDL
            \partEL
            \partFL
        }
    >>
}
