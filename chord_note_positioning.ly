%; Procedure for offsetting noteheads in a chord horizontally to avoid collisions.
%; Turns out that the placement of noteheads becomes quite involved
%; if you want the chord layouts to follow recognizable patterns.
%;
%; The placement algorithm follows these priorities:
%; 1. The end of the stem should attach to a notehead pointing in the correct direction.
%; 2. Notes should be sorted by lowest pitch from left to right, bottom to top.
%; 3. The placement should take up as few columns as possible.
%; 4. Related notes should be placed together.
%;    Notes are related by their whole-tone scale
%;    and by whether or not they reside on a staff line.
%; 5. Prefer attaching noteheads to the stem when possible.
%; 6. If not directly attached to the stem,
%;    notes should instead be attached to another notehead at the exact same height.
%;
%; This algorithm places heads into four columns centered around the stem.
%; If all noteheads can fit into one column, it doesn't run at all.
%;
%; 1. Notes are sorted into four initial columns based on their pitch (modulo 4).
%;   C  -> column 0
%;   C# -> column 1
%;   D  -> column 2
%;   D# -> column 3
%;   E  -> column 0
%;   ...
%; In order to ensure priority #1,
%; the arrangement is offset by 2 if the primary notehead falls on a staff line.
%; If the stem direction is DOWN, the offset occurs if the primary notehead doesn't fall on a line.
%; This sorting also ensures priority #2 and #4.
%;
%; 2. Free-floating noteheads are moved a column inwards (0 -> 1, 3 -> 2).
%; Now all noteheads touch either the stem or another notehead.
%; Helps to satisfy #3 and #5
%;
%; 3. Attached notehead pairs are centered around the stem, when this doesn't cause a collision.
%; Helps to satisfy #3 and #5.
%;
%; Now that the column offsets have been calculated as integers,
%; the heads are then appropriately translated on the X axis.

#(define (custom-note-positioning) (lambda (grob)
  ;;;;; Gather noteheads and data ;;;;;
  (let* (
    (stem-dir (ly:grob-property grob 'direction)) ; 1 is UP, -1 is DOWN
    (note-heads (ly:grob-array->list (ly:grob-object grob 'note-heads)))

    (stem-thickness (* (ly:staff-symbol-line-thickness grob) (ly:grob-property grob 'thickness)))
    (note-width ((lambda (extent) (abs (- (car extent) (cdr extent))))
      (ly:grob-property (car note-heads) 'X-extent)))

    (note-pitches '())
    (note-heights '())
    (note-height-parity 0)
    (note-horizontal-offsets '())

    (old-collision-threshold (ly:grob-property grob 'note-collision-threshold))
  )
    ; sort noteheads, lowest pitch first
    (set! note-heads (sort note-heads
      (lambda (head1 head2) (<
        (ly:pitch-semitones (ly:event-property (event-cause head1) 'pitch))
        (ly:pitch-semitones (ly:event-property (event-cause head2) 'pitch))
      ))
    ))

    ; acquire note pitches and vertical positions
    (set! note-pitches (map (lambda (head) (
        ly:pitch-semitones (ly:event-property (event-cause head) 'pitch)
    )) note-heads))

    (set! note-heights (map
        (lambda (head) (ly:grob-property head 'staff-position))
        note-heads
    ))

    ;;;;; Calculate notehead offsets ;;;;;
    ; only offset notes if they cannot fit in a single column
    ; in other words, if all notes are at least 2 staff slots apart
    (if (any (lambda (height1 height2)
        (< (abs (- height1 height2)) 2)) note-heights (cdr note-heights))
      (begin
        (set! note-heights (list->array 1 note-heights))

        ; parity decides whether a note should be on the left or right of stem when on a staff line
        (set! note-height-parity
          (abs (-
            (if (= 1 stem-dir) 0 1)
            (modulo (array-ref note-heights
              (if (= 1 stem-dir) 0
                (- (car (array-dimensions note-heights)) 1)))
              2))))


        ; 1. place notes into initial 4 columns
        (set! note-horizontal-offsets (list->array 1 (map
          (lambda (pitch) (modulo (+ pitch (* 2 note-height-parity)) 4))
          note-pitches)))

        ; TODO: Make a function for the collision query.
        ; Well, if I knew how to create an inline lambda that fully executed at every call.
        ; Scheme is... interesting in that regard.

        ; 2. push outer noteheads to touch stem when possible
        (for-each (lambda (idx)
          (let* ((col (array-ref note-horizontal-offsets idx)))
            ; left side
            (if (and (= col 0) (or
              (not (array-in-bounds? note-horizontal-offsets (+ idx 1)))
              (not (= (array-ref note-heights idx) (array-ref note-heights (+ idx 1))))))
                (array-set! note-horizontal-offsets 1 idx)
            )
            ; right side
            (if (and (= col 3) (or
              (not (array-in-bounds? note-horizontal-offsets (- idx 1)))
              (not (= (array-ref note-heights idx) (array-ref note-heights (- idx 1))))))
                (array-set! note-horizontal-offsets 2 idx)
            )
          ))
          (iota (car (array-dimensions note-horizontal-offsets))))

        ; 3. push notehead pairs (two heads at the same height) to center around stem when possible
        (for-each (lambda (idx)
          (let* ((col (array-ref note-horizontal-offsets idx)))
            ; left side
            (if (and
              (= col 0)
              (or (not (array-in-bounds? note-horizontal-offsets (+ idx 2)))
                  (not (= (+ 1 (array-ref note-heights idx)) (array-ref note-heights (+ idx 2)))))
              (or (not (array-in-bounds? note-horizontal-offsets (- idx 1)))
                  (not (= (- 1 (array-ref note-heights idx)) (array-ref note-heights (- idx 1)))))
              )
              (begin
                (array-set! note-horizontal-offsets 1 idx)
                (array-set! note-horizontal-offsets 2 (+ idx 1))
              )
            )
            ; right side
            (if (and
              (= col 3)
              (or (not (array-in-bounds? note-horizontal-offsets (- idx 2)))
                  (not (= (- 1 (array-ref note-heights idx)) (array-ref note-heights (- idx 2)))))
              (or (not (array-in-bounds? note-horizontal-offsets (+ idx 1)))
                  (not (= (+ 1 (array-ref note-heights idx)) (array-ref note-heights (+ idx 1)))))
              )
              (begin
                (array-set! note-horizontal-offsets 2 idx)
                (array-set! note-horizontal-offsets 1 (- idx 1))
              )
            )
          ))
          (iota (car (array-dimensions note-horizontal-offsets))))

        ; Columns currently range from 0 to 3, readjust these offsets to center around stem.
        ; The stem is placed on either the left or right side of the primary head,
        ; depending on its direction.
        ; The correction is correspondingly either -2 or -1.
        (set! note-horizontal-offsets (map
          (lambda (offset) (- offset (if (= stem-dir 1) 1 2)))
          (array->list note-horizontal-offsets)))

        ;;;;; Move noteheads ;;;;;
        ; Run the built-in note head positioning in order to align note heads to the stem.
        ; Set collision threshold so low
        ; that no note heads are moved to the opposite side of the stem.
        (ly:grob-set-property! grob 'note-collision-threshold -1)
        (ly:stem::calc-positioning-done grob)
        (ly:grob-set-property! grob 'note-collision-threshold old-collision-threshold) ; restore

        ; Move the noteheads according to the offsets.
        ; Currently, they are moved by `(notewidth - stemwidth) * offset`,
        ; though it might be a good idea to implement some nudging here.
        (for-each
          (lambda (head offset)
            ; we don't perform the translation if the offset is 0
            (if (not (= 0 offset))
              (ly:grob-translate-axis!
                head
                (* (- note-width stem-thickness) offset)
                X)))
          note-heads note-horizontal-offsets)
      ))
    #t)
))
