\version "2.22.0"
\include "alternative_notation.ly"

\header {
    title = "A Cruel Angel's Thesis"
    composer = "Shiro Sagisu"
    instrument = "Piano"
    arranger = "Arr.: T. Korreman"
}

%%% Intro
iR = \relative c'' {
    \tempo "Slightly slowing down" c4(\<\p es f8. es f8 |
    f f bes as g16 f8 g8.) r8 |
    g4( bes c8. g f8 |
    bes8 bes g bes bes8.\fermata c8.~\fermata c8)\! |
}

iL = \relative c' {
    \clef treble
    <es g c>2 <f as c> |
    <f bes d> <g bes es> |
    <g c es> <as c f> |
    <f bes d> <es as c es> |
}

%%% Intro 2

jR = \relative c'' {
    \tempo "Fast & Upbeat"
    c4 \f es f8.( es f8-.) |
    f-. f-. bes-. as-. g16-. f8-. g16-. r4 |
    g4 bes c8.( g f8) |
    bes8-. bes-. g-. bes-. bes8.( c8-.) r8. |
}

jL = \relative c {
    \clef bass
    <c g'>4 q <f c'> q |
    <bes, f'> q <es bes'> q |
    <c g'> q <f c'> q |
    <bes, f'> q <c g'>2 |
}

%%% Verse
vR = \relative c'' {
    \tempo "Moderate"
    r4 \mp es8 bes8 bes4. es8( |
    es8. f bes,8 bes4.) bes8( |
    g'8. as g8 f8. es f8 |
    g8. as g8 c,4.) c16( d |

    es8. es d8 d4.) es16( f |
    as8. g f8 es4.) g8 |
    g8.( f e8 f4) c |
    c4.( d8) d2 |

    r4 es8. (bes16 ~bes4.) es8( |
    es8. f bes,8 bes4.) bes8( |
    g'8. as g8 f8. es f8 |
    g8. as g8 c,4.) c16( d |

    es8. es d8 d4.) es16( f |
    as8. g f8 es4.) g8 |
    g8.( f e8 f8. g as8 |
    <<{g4)^~ g^~ g^~ g} {s4 <b, d>4( <c es> <d f>}>> |
}

vL = \relative c, {
    <es es'>1 |
    <bes' f'> |
    <c g'>2 <bes f'> |
    <as es'>1 |

    <bes f'>2~ <bes f' bes> |
    <es bes'> <c g'> |
    <d f c'>1 |
    <g, d'>2~ <g d' g> |

    <es bes'>1 |
    <bes' f'> |
    <c g'>2 <bes f'> |
    <as es'>1 |

    <bes f'>2~ <bes f' bes> |
    <es bes'> <c g'> |
    <d f c'>2~ <d f as c> |
    g,4( d' g d) |
}

%%% Buildup

bR = \relative c'' {
    \tempo "Building up"
    es8.)( es d8 es8. es d8 |
    f8. f es8 d8. c d8 |
    es8. es d8 f8. d c8 |
    bes4) g'4( as bes |

    <es, c'>8.)(\< es d8 es8. es d8 |
    f8. f es8 d8. es f8 |
    g8. as g8 f8. es f8 |
    <b, d g>2)\f g'8.^2( a^3 b8^4 |
}

bL = \relative c {
    <as es'>4 q q q |
    <g es'> q <c g'> q |
    <f, c'> q <bes f'> q |
    <bes g'>4 bes' <es, des'>4 bes |

    <as es'>4 q q q |
    <g es'> q <c g'> q |
    <d, d'>2 <f' as c> |
    <g, d' g>1 |
}

%%% Chorus
cR = \relative c'' {
    <c^1 c'^5>4\accent) \tempo "Faster, go all out" es^2 f8.( es f8) |
    f f bes as g16 f8 g16 r4 |
    g4 bes c8.( f, es8) |
    d d c d f16 es8 es16 r4 |

    c4 es f8.( es f8) |
    f f bes as g16 f8 g16 r4 |
    g4 bes c8.( f, es8) |
    bes' bes g bes bes8.( c8) r8. |

    c,4 es f8.( es f8) |
    f f bes as g16 f8 g16 r4 |
    g4 bes c8.( f, es8) |
    bes' bes g bes bes8.( c8) r8. |
}

cL = \relative c {
    <c, c'>2 <f c' f>4 q |
    <bes f' bes> q <es bes' es> q|
    <c g' c> q <f, c' f> q |
    <bes f' bes> q <es bes' es> q8 <d d'> |

    <c g' c>4 q <f, c' f> q |
    <bes f' bes> q <es bes' es> q |
    <c g' c>4 q <f, c' f> q |
    <bes f' bes> q <c g' c>2 |

    <c, c'>2 <f c' f>4 q |
    <bes f' bes> q <es, bes' es> q |
    <c c'>2 <f c' f>4 q |
    <bes f'>8_1_4 q <g d'>_2_5 <bes f'> q8.( <c g'>8_1_3) r8. |
}

\score {
    \context GrandStaff <<
        \new KorrStaff { \clef treble
            \numericTimeSignature \time 4/4
            \key c \minor

            \iR \break
            \jR \break

            \vR
            \bR

            \cR
        }
        \new KorrStaff {
            \numericTimeSignature \time 4/4
            \key c \minor

            \iL
            \jL

            \vL
            \bL

            \cL
        }
    >>
}
