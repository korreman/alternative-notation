\version "2.22.1"

%; overrides the staff stencil with a new one
%; this staff has 7 lines, of which the bottom, middle, and top are highlighted
staffStencil =
#(define-music-function (parser location) ()
#{
  %; ensure that symbols are drawn over the staff
  \override Staff.StaffSymbol.layer = #-1
  \override StaffSymbol.line-positions = #'(-6 -4 -2 0 2 4 6)
  \override Staff.StaffSymbol.after-line-breaking =
    #(lambda (grob)
      (let* (; query staff properties
            (staff-stencil (ly:grob-property grob 'stencil))
            (staff-width (interval-length (ly:stencil-extent staff-stencil X)))
            (staff-space (ly:staff-symbol-staff-space grob))
            (staff-line-thickness (ly:staff-symbol-line-thickness grob))

            ; line stencils to build staff from
            (black-stencil (make-line-stencil (* 1.5 staff-line-thickness) 0 0 staff-width 0))
            (gray-stencil (ly:stencil-in-color
              (make-line-stencil staff-line-thickness 0 0 staff-width 0)
              0.6 0.6 0.6))

            ; lists of lines and their colors
            (line-list '(#f #t #t #f #t #t #f))
            (staff-line-positions '(-6 -4 -2 0 2 4 6))

            ; new staff-symbol
            (new-stil
              (apply ly:stencil-add
                (map
                  (lambda (pos is-weak)
                    (ly:stencil-translate
                      (if (eq? is-weak #f) black-stencil gray-stencil)
                      (cons (/ staff-line-thickness 2) (* (/ pos 2) staff-space))))
                  staff-line-positions
                  line-list))))

    ; apply the staff symbols
    (ly:grob-set-property! grob 'stencil new-stil)))
#})

%; Choose between black or white noteheads depending on which whole-tone scale the pitch falls into
%; Graphical Object -> Stencil
#(define (coloredPitchHeads) (lambda (grob)
    (let*
        (   ; acquire music font
            (notefont (ly:grob-default-font grob))
            ; retrieve the note event that created grob, use to get pitch
            (pitch (ly:event-property (event-cause grob) 'pitch))
            (duration (ly:grob-property grob 'duration-log))
            (semitones (ly:pitch-semitones pitch))
        )
        ; choose a notehead glyph based on the evenness of pitch
        (if (= duration 0)
            (case (modulo semitones 2)
                ((0) (ly:font-get-glyph notefont "noteheads.s1laThin")) ; hollow
                ((1) (ly:font-get-glyph notefont "noteheads.s2laThin")) ; full
            )
            (case (modulo semitones 2)
                ((0) (ly:font-get-glyph notefont "noteheads.s1sol")) ; hollow
                ((1) (ly:font-get-glyph notefont "noteheads.s2sol")) ; full
            )
        )
    )
))

%; vertical note placement: two semitones per staff line
%; achieved by dividing pitch by two and rounding down
%; Int -> Int
twinNotePitchLayout = #(lambda (p) (floor (/ (ly:pitch-semitones p) 2)))

customDuration = #(lambda (grob)
    (let ( (dur (ly:duration-log (ly:event-property (event-cause grob) 'duration))))
        ;(newline) (display dur)
        (if (= 1 dur) 0 dur)
    )
)

% TODO:
% * Durations
%   - The relationship between `Stem` visibility and duration is hardcoded.
%     This code is referenced all over the place.
%     Maybe override the duration while a stem is handled, then revert once done?
%     Also, what if I want to use new symbols?
% * Intelligent note placement - DONE
% * Ottavas don't offset notes by a whole octave
% * Rest symbols
% * Key signatures
% * Time signatures
% * 1, 3, 4, 5 octave staffs
% * Create a table of musical term translations
% * Possibly: better handling of glyph replacement
% * Possibly: replace staccato symbol

\include "chord_note_positioning.ly"
\layout {
    \context {
        \Staff
        \name KorrStaff
        \alias Staff

        \remove "Accidental_engraver"
        \remove "Key_engraver"

        \staffStencil
        \numericTimeSignature

        \override NoteHead #'stencil = #(coloredPitchHeads)
        staffLineLayoutFunction = \twinNotePitchLayout
        \override Stem #'positioning-done = #(custom-note-positioning)
        \override Stem #'duration-log = \customDuration
    }

    \context { \Score \accepts KorrStaff }
    \context { \ChoirStaff \accepts KorrStaff }
    \context { \GrandStaff \accepts KorrStaff }
    \context { \PianoStaff \accepts KorrStaff }
    \context { \StaffGroup \accepts KorrStaff }
}
