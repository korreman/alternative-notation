\version "2.22.1"
\include "alternative_notation.ly"
\score {
  \pointAndClickOff
  \new GrandStaff <<
    \new KorrStaff \relative c' { \clef treble
      c cis d dis | e f fis g | gis a ais b |
      c cis d dis | e f fis g | gis a ais b |
      <c,, e g> <d f a> <c cis f gis> <fis a b cis e> |
      <c' cis f gis> <d, f aes cis> <dis fis a d> <c cis d> |
      <c cis d dis e f fis g gis a ais b c> <cis d fis a> r r |
    }
  >>
}
